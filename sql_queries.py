# DROP TABLES
location_table_drop = "DROP table IF EXISTS Location;"
camera_table_drop = "DROP table IF EXISTS Camera;"
source_video_table_drop = "DROP table IF EXISTS Source_Video;"
trimmed_video_table_drop = "DROP table IF EXISTS Trimmed_Video;"
counted_vehicle_table_drop = "DROP table IF EXISTS Counted_Vehicle;"
source_events_table_drop = "DROP table IF EXISTS Source_Events;"
backup_events_table_drop = "DROP table IF EXISTS Backup_Events;"
stage_events_table_drop = "DROP table IF EXISTS Stage_Events;"
trim_events_table_drop = "DROP table IF EXISTS Trim_Events;"

# CREATE TABLES

location_table_create = ("""
CREATE TABLE IF NOT EXISTS Location (
    location_id INTEGER NOT NULL PRIMARY KEY,
    location_name VARCHAR NOT NULL,
    barangay_id INTEGER NOT NULL,
    barangay_name VARCHAR NOT NULL,
    city_id INTEGER NOT NULL,
    city_name VARCHAR NOT NULL,
    province_id INTEGER NOT NULL,
    province_name VARCHAR NOT NULL,
    region_id INTEGER NOT NULL,
    region_name VARCHAR NOT NULL
    );
""")

camera_table_create = ("""
CREATE TABLE IF NOT EXISTS Camera (
    camera_id INTEGER PRIMARY KEY, 
    client_id VARCHAR NOT NULL,
    location_id INTEGER NOT NULL,
    camera_area VARCHAR(25) NOT NULL,
    camera_type VARCHAR(10) NOT NULL,
    camera_fps INTEGER NOT NULL,
    camera_resolution INTEGER [] NOT NULL,
    traffic_flow_direction VARCHAR [],
    violation_zone JSON,
    count_reference_line JSON,
    ts_created TIMESTAMP NOT NULL,
    ts_updated TIMESTAMP NOT NULL,
    user_id INTEGER NOT NULL
    );
""")

source_video_table_create = ("""
CREATE TABLE IF NOT EXISTS Source_Video (
    source_video_id SERIAL NOT NULL PRIMARY KEY,
    video_name VARCHAR NOT NULL,
    client_id INTEGER NOT NULL,
    camera_id INTEGER NOT NULL,
    file_size VARCHAR NOT NULL,
    duration INTEGER NOT NULL,
    dt_recorded VARCHAR,
    dt_uploaded VARCHAR,
    ai_process VARCHAR,
    ts_processed TIMESTAMP,
    dt_processed VARCHAR
    );
""")

trimmed_video_table_create = ("""
CREATE TABLE IF NOT EXISTS Trimmed_Video (
    trimmed_video_id SERIAL NOT NULL PRIMARY KEY, 
    source_video_id INTEGER NOT NULL,
    trimmed_path VARCHAR NOT NULL,
    client_id INTEGER NOT NULL,
    camera_id INTEGER NOT NULL,
    file_size VARCHAR NOT NULL,
    duration INTEGER NOT NULL,
    ts_trimmed TIMESTAMP,
    dt_trimmed VARCHAR
    );
""")

counted_vehicle_table_create = ("""
CREATE TABLE IF NOT EXISTS Counted_Vehicle (
    vehicle_id SERIAL NOT NULL PRIMARY KEY, 
    track_id INTEGER NOT NULL, 
    location_id INTEGER NOT NULL,
    source_video_id INTEGER NOT NULL,
    vehicle_type VARCHAR NOT NULL,
    vehicle_direction VARCHAR NOT NULL,
    ts_counted TIMESTAMP NOT NULL
    );
""")

source_events_table_create = ("""
CREATE TABLE IF NOT EXISTS Source_Events (
    video_id SERIAL NOT NULL PRIMARY KEY,
    video_name VARCHAR NOT NULL,
    client_id INTEGER NULL,
    camera_id INTEGER NOT NULL,
    file_size VARCHAR NOT NULL,
    duration INTEGER NOT NULL,
    ts_uploaded TIMESTAMP,
    dt_uploaded VARCHAR,
    user_id INTEGER
    );
""")

backup_events_table_create = ("""
CREATE TABLE IF NOT EXISTS Backup_Events (
    video_name VARCHAR NOT NULL PRIMARY KEY,
    video_id INTEGER NOT NULL,
    client_id INTEGER NOT NULL,
    camera_id INTEGER NOT NULL,
    video_path VARCHAR NOT NULL,
    file_size VARCHAR NOT NULL,
    duration INTEGER NOT NULL,
    ts_backup TIMESTAMP,
    dt_backup VARCHAR,
    user_id INTEGER
    );
""")

stage_events_table_create = ("""
CREATE TABLE IF NOT EXISTS Stage_Events (
    video_name VARCHAR NOT NULL PRIMARY KEY,
    video_id INTEGER NOT NULL,
    client_id INTEGER NULL,
    camera_id INTEGER NOT NULL,
    video_path VARCHAR NOT NULL,
    file_size VARCHAR NOT NULL,
    duration INTEGER NOT NULL,
    ts_staged TIMESTAMP,
    dt_staged VARCHAR
    );
""")

trim_events_table_create = ("""
CREATE TABLE IF NOT EXISTS Trim_Events (
    video_name VARCHAR NOT NULL PRIMARY KEY,
    video_id INTEGER NOT NULL,
    client_id INTEGER NULL,
    camera_id INTEGER NOT NULL,
    video_path VARCHAR NOT NULL,
    file_size VARCHAR NOT NULL,
    duration INTEGER NOT NULL,
    ts_trimmed TIMESTAMP,
    dt_trimmed VARCHAR
    );
""")

# INSERT RECORDS

location_table_insert = "INSERT INTO Location (location_id, location_name, barangay_id, barangay_name, city_id, city_name, province_id, province_name, region_id, region_name) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT (location_id) DO NOTHING"

camera_table_insert = "INSERT INTO Camera (camera_id, client_id, location_id, camera_area, camera_type, camera_fps, camera_resolution, traffic_flow_direction, violation_zone, count_reference_line, ts_created, dt_created, user_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT (camera_id) DO NOTHING"

source_video_table_insert = "INSERT INTO Source_Video (source_video_id, video_name, client_id, camera_id, file_size, duration, ts_recorded, ts_uploaded, ai_process, ts_processed, dt_processed) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT (source_video_id) DO NOTHING"

trimmed_video_table_insert = "INSERT INTO Trimmed_Video (trimmed_video_id, source_video_id, trimmed_path, client_id, camera_id, file_size, duration, ts_trimmed, dt_trimmed) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT (trimmed_video_id) DO NOTHING"

counted_vehicle_table_insert = "INSERT INTO Counted_Vehicle (vehicle_id, track_id, location_id, source_video_id, vehicle_type, vehicle_direction, ts_counted) VALUES (%s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT (vehicle_id) DO NOTHING"

source_events_table_insert = "INSERT INTO Source_Events (video_name, client_id, camera_id, file_size, duration, ts_uploaded, dt_uploaded, user_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"

backup_events_table_insert = "INSERT INTO Backup_Events (video_name, video_id, client_id, camera_id, video_path, file_size, duration, ts_backup, dt_backup,user_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s,%s)"

stage_events_table_insert = "INSERT INTO Stage_Events (video_name, video_id, client_id, camera_id, video_path,file_size, duration, ts_staged, dt_staged) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"

trim_events_table_insert = "INSERT INTO Trim_Events (video_name, video_id, client_id, camera_id, video_path, file_size, duration, ts_trimmed, dt_trimmed) VALUES (%s,%s, %s, %s, %s, %s, %s, %s, %s)"


# QUERY LISTS

create_table_queries = [location_table_create, camera_table_create, source_video_table_create, trimmed_video_table_create, counted_vehicle_table_create, source_events_table_create, backup_events_table_create, stage_events_table_create, trim_events_table_create]
drop_table_queries = [location_table_drop, camera_table_drop, source_video_table_drop, trimmed_video_table_drop, counted_vehicle_table_drop, source_events_table_drop, backup_events_table_drop, stage_events_table_drop, trim_events_table_drop]