import cv2, time, os
import moviepy.editor as mpy
import ffmpy
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
import subprocess
import math
import psycopg2
from sql_queries import *
upload_to_sql = True
""" requirements
conda install -c conda-forge moviepy
or 
pip install moviepy

pip install ffmpy
sudo apt-get install postgresql-client
pip install psycopg2-binary
"""
sql_host = "192.168.1.12"
time_per_trim = 60

def get_fps(video):
    fps = video.get(cv2.CAP_PROP_FPS)
    return fps
def get_size(path):
    "returns size of the file indicated in path in string format \"XX B\""
    return f"{os.stat(path).st_size} B"
def select_data(cur, conn):
    select_query = "SELECT * FROM Source_Events ORDER BY video_id DESC LIMIT 1;"
    cur.execute(select_query)
    record = cur.fetchone()
    return record
def trim_one_minute(watcher, video_path,dest,video_id):
    "watcher, video_path, dest, video_id"
    duration = get_duration(video_path)
    number_of_vids = math.ceil(duration/60)
    print(number_of_vids)

    for vid_num in range(number_of_vids):
        trim(watcher, video_path,vid_num,dest,video_id)
    camera_folder = dest.split("/")[1]
    print("DEST: ",dest)
    print("CAMERA FOLDER: " , camera_folder)
    #os.system(f"sshpass -p \"Memyself#20\" rsync -avhuP {camera_folder} mari.amon@192.168.1.115:/home/backupvm/APMC $$ echo 'RSYNC DONE!' ")

def trim(watcher, vid_path, vid_num,camera_folder,video_id):
    """
    watcher, vid_path, vid_num,camera_folder(destination),video_id
    """
    names = watcher.gen_filename_all(vid_path) #get all names for code simplicity

    str_vid_num = f"{vid_num}".zfill(3)
    str_vid_num = str_vid_num.zfill(3)
    
    vid_name = names[6] #catch all file name 

    catch_all_filename_only = vid_name.split(".")[0]
    extension  = vid_name.split(".")[1]

    ffmpeg_extract_subclip(vid_path, vid_num*time_per_trim, (vid_num+1)*time_per_trim, 
        targetname=f"{camera_folder}/{catch_all_filename_only}_{str_vid_num}.{extension}")

    #info to be uploaded to sql
    trimmed_path = f"{camera_folder}/{catch_all_filename_only}_{str_vid_num}.{extension}"
    file_size = get_size(trimmed_path)
    duration = get_duration(trimmed_path)
    trimmed_video_name = f"{catch_all_filename_only}_{str_vid_num}.{extension}"
    video_name  = f'{names[2]}_{str_vid_num}'

    if(upload_to_sql):
        upload_sql_trim_events(video_id,video_name,trimmed_video_name,f"{camera_folder}",0,names[4],file_size,duration,watcher.get_time())
                    

def get_duration(vid_file, mpeg=False, fps=30): 
    """Get the duration of a video in seconds."""
    try:
        cmd = 'ffprobe -i {} -show_entries format=duration -v quiet -of csv="p=0"'.format(vid_file)
        output = subprocess.check_output(
            cmd,
            shell=True,  # Let this run in the shell
            stderr=subprocess.STDOUT
        )
        length = float(output)
        mpeg = True
    except:
        cap = cv2.VideoCapture(vid_file)
        (major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
        if int(major_ver) < 3:
            fps = cap.get(cv2.cv.CV_CAP_PROP_FPS)
            frame_count = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
        else:
            fps = cap.get(cv2.CAP_PROP_FPS)
            frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        
        if(fps == 0):
            fps = 30
        length = frame_count/fps
        mpeg = False
    return length


def upload_sql_trim_events(video_id, video_name, trimmed_video_name, trimmed_directory, client_id, camera_id, file_size, duration, created_at):
    """
    log trim events to SQL database 
    video_id, video_name, trimmed_video_name, trimmed_directory, client_id, camera_id, file_size, duration, created_at
    """
    try: 
        video_name = video_name.split(".")[0]
    except:
        print("video_name has no extension")

    ts_trimmed = created_at
    dt_trimmed = created_at
    video_path = trimmed_directory+trimmed_video_name
    command = trim_events_table_insert  
    #"INSERT INTO Trim_Events (video_name, video_id, client_id, camera_id, video_path, file_size, duration, ts_trimmed, dt_trimmed) VALUES (%s,%s, %s, %s, %s, %s, %s, %s, %s)"

    x = [video_name, video_id, client_id, camera_id, video_path, file_size, duration, ts_trimmed, dt_trimmed] #9 args 
    sql_execute(command,x)

def upload_sql_source_video(video_name, client_id, camera_id, file_size, duration, ts_uploaded, dt_uploaded, user_id):
    command = source_events_table_insert
    x = [video_name, client_id, camera_id, file_size, duration, ts_uploaded, dt_uploaded, user_id]
    sql_execute(command,x)
    video_id = sql_get_last_video_id()
    return video_id

def upload_sql_staged_video(video_name, video_id, client_id, camera_id,video_path, file_size, duration, ts_staged, dt_staged):
    """
    log stage (renaming) events to SQL database 
    video_name, video_id, client_id, camera_id,video_path, file_size, duration, ts_staged, dt_staged
    """
    command = stage_events_table_insert
    #"INSERT INTO Stage_Events (video_name, video_id, client_id, camera_id, file_size, duration, ts_staged, dt_staged) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
    x = [video_name, video_id, client_id, camera_id, video_path,file_size, duration, ts_staged, dt_staged]
    sql_execute(command,x)
def upload_sql_backup_video(video_name, video_id, client_id, camera_id, video_path, file_size, duration, ts_backup, dt_backup,user_id):
    command = backup_events_table_insert
    #backup_events_table_insert = "INSERT INTO Backup_Events (video_name, video_id, client_id, camera_id, video_path, file_size, duration, ts_backup, dt_backup,user_id) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s,%s)"
    x = [video_name, video_id, client_id, camera_id, video_path, file_size, duration, ts_backup, dt_backup,user_id]
    sql_execute(command,x)

def sql_get_last_video_id():
    conn = psycopg2.connect(f"host={sql_host} dbname=catchalldb user=postgres password=docker port=5432")
    cur = conn.cursor()
    select_query = "SELECT * FROM Source_Events ORDER BY video_id DESC LIMIT 1;"
    cur.execute(select_query)
    record = cur.fetchone()
    conn.close()
    return record[0]

def sql_execute(command, params):
    conn = psycopg2.connect(f"host={sql_host} dbname=catchalldb user=postgres password=docker port=5432")
    cur = conn.cursor()
    x = params
    cur.execute(command, x)
    conn.commit()
    conn.close()
def compress(input_name):
    
    crf = 24 #18 -24
    output_name = f"compressed_{input_name}"
    inp={input_name:None}
    outp = {output_name:'-vcodec libx264 -crf %d'%crf}
    ff=ffmpy.FFmpeg(inputs=inp,outputs=outp)
    print(ff.cmd) # just to verify that it produces the correct ffmpeg command
    ff.run()
    print("done compressing!")

def main():
    #get_duration("twice.mp4")
    #trim("twice.mp4",0)
    trim_one_minute("twice.mp4")

if __name__ == "__main__":
    main()