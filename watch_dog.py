
import time, os, datetime,re
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
from staging import *
import shutil

class Watcher:
    csv_format = False
    curr_time = None
    log = None
    file_types = "mp4","avi"
    path_to_watch = "drive"
    upload_to_sql = True
    

    def __init__(self, path_to_watch, *filetypes):
        self.path_to_watch = path_to_watch
        if(len(filetypes)>0):
            self.file_types = filetypes
        try:
            print('INIT BACKUP')
            if(self.csv_format):
                self.log = open("logs/logs.csv","a+")
            else:
                self.log = open("logs/logs.txt","a+") 
        except:
            os.mkdir("logs")
            extension = "csv" if self.csv_format else "txt"
            log_path = f"logs/logs.{extension}"
            self.log = open(log_path,"w+")
            self.log.close()
            self.log = open(log_path,"a+")

    def on_created(self,event):
        
        time.sleep(5)
        
        #filename = self.gen_filename(event.src_path,1)
        print(f"created, {event.src_path}, {self.get_time()}\n")
        self.log.write(f"created, {event.src_path}, {self.get_time()}\n")
        self.log.flush()

    def on_deleted(self,event):
        print(f"deleted, {event.src_path}, {self.get_time()}\n")
        self.log.write(f"deleted, {event.src_path}, {self.get_time()}\n")
        self.log.flush()

    def on_modified(self,event):    
        print(f"modified, {event.src_path}, {self.get_time()}\n")
        

        if(self.check_file_name(event.src_path)):
            video_id = 0
            print(self.gen_filename(event.src_path,5))
            file_name = self.gen_filename(event.src_path,2)
            self.terminal(f'copy {event.src_path} BACKUP/{file_name}.mp4')
            camera_id = str(int(self.gen_filename(event.src_path,4)))
            video_path = f'BACKUP/{file_name}'
            time_created = self.get_time()
            file_size = get_size(event.src_path)
            duration = get_duration(event.src_path)
            upload_sql_backup_video(file_name,video_id,0,camera_id,video_path,file_size,duration,time_created,time_created,11772751)
            #(catchall_name,video_id,0,camera_id,video_path,file_size,duration,time_created,time_created) 
            print("BACKING UP OF FILE COMPLETE")
        self.log.write(f"modified, {event.src_path}, {self.get_time()}\n")
        self.log.flush()

    def on_moved(self,event):
        self.log.write(f" Moved, {event.src_path} to {event.dest_path}, {self.get_time()}\n")
        self.log.flush()

    def gen_filename(self, path, index):
        """ 
            returns all names needed as a list
            0 = full path                       CatchALL/ch05_20190813150000.mp4
            1 = full filename                   ch05_20190813150000.mp4
            2 = filename without extension      ch05_20190813150000
            3 = extension                       mp4
            4 = camera number                   05
            5 = camera folder                   20190813_1500
            6 = catch all name                  ch005_20190813_1500.mp4
        """
        return self.gen_filename_all(path)[index]

    def gen_filename_all(self, path):
        """ 
            returns all names needed as a list
            0 = full path                       CatchALL/ch05_20190813150000.mp4
            1 = full filename                   ch05_20190813150000.mp4
            2 = filename without extension      ch05_20190813150000
            3 = extension                       mp4
            4 = camera number                   05
            5 = camera folder                   20190813_1500
            6 = catch all name                  ch005_20190813_1500.mp4
        """
        names = []
        
        names.append(path)                          #full path              [0]       CatchALL/ch05_20190813150000.mp4
        names.append(names[0].split("/")[-1])       #full file name         [1]       ch05_20190813150000.mp4
        names.append(names[1].split(".")[0])        #filename w/o extension [2]       ch05_20190813150000
        names.append(names[1].split(".")[-1])       #extension              [3]       mp4
        names.append(names[1][2:4])                 #camera number          [4]       05
        names.append(names[2].split("_")[-1][:8]+"_"+names[2].split("_")[-1][8:12])     #camera folder [5] 20190813_1500
        names.append("ch0"+names[4]+"_"+names[5]+"."+names[3])                          #catchall name [6] ch005_20190813_1500.mp4

        #print(names)
        return names

    def get_time(self):
        "returns time in SQL timestamp format"
        return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
 
    def process_new_file(self,event):
        """
        self,event
        updates sql table and gets the current video_id
        """
        if(self.check_file_name(event.src_path)):
            print(f'{event.src_path} WILL BE PROCESSED')
            old_filename =  self.gen_filename(event.src_path,1)
            camera_id = self.gen_filename(event.src_path,4)

            if(self.upload_to_sql):
                video_id = upload_sql_source_video(old_filename,0,camera_id,get_size(event.src_path),get_duration(event.src_path),self.get_time(),self.get_time(),11772751)
            else:
                video_id = 0

            new_filename =  self.gen_filename(event.src_path,6) #catchall name format       chXXX_XXXXXXXX_XXXX.mp4 

            try:
                os.mkdir(f"APMC/camera0{self.gen_filename(event.src_path,4)}")
            except:
                print("folder APMC already exist")

            try:
                camera_number_folder = f"camera0{self.gen_filename(event.src_path,4)}"
                os.mkdir(f"APMC/{camera_number_folder}/{self.gen_filename(event.src_path,5)}")
            except:
                print(f"folder {self.gen_filename(event.src_path,5)} already exist")


            try:
                print('trying to copy')
                shutil.copy2(old_path,f"APMC/{new_filename}")
                
            except:
                
                print('error copying to ',f"APMC/{new_filename}")
        return video_id
                
    def check_file_name(self, path):
        """
        check if the filename satisfies the customers' format
        path must be in the format CatchALL/chXX_XXXXXXXXXXXXXX.mp4
        """
        file_name = self.gen_filename(path,1)
        if(re.match(r"ch[0-9]{2}_[0-9]{14}", file_name)):
            #match
            print('FILENAME MATCHED!')
            return True
        else:
            print('FILENAME DO NOT MATCH CLIENT NAME SCHEME')
            return False

    def terminal(self,command):
        """
        Detects the current Operating System.
        Sends the command to the terminal/command line accordingly
        """
        
        if(os.name == 'nt'):
            print('Detected Windows OS')
            os.system(command)
        else:
            print('Detected Linux-based OS')
            command = "cp "+command[5:]
            os.system(command)
    def run(self):
        """
        Running the watcher
        """
        self.curr_time = self.get_time()
        patterns = "ch*"
        ignore_patterns = "*/logs.txt", "*/logs.csv","*/logs","*.py"
        ignore_directories = True
        case_sensitive = True
    
        my_event_handler = PatternMatchingEventHandler(patterns, ignore_patterns, ignore_directories, case_sensitive)
        my_event_handler.on_created = self.on_created
        my_event_handler.on_deleted = self.on_deleted
        my_event_handler.on_modified = self.on_modified
        my_event_handler.on_moved = self.on_moved
        path = self.path_to_watch
        go_recursively = True
        my_observer = Observer()
        my_observer.schedule(my_event_handler, path, recursive=go_recursively)
        my_observer.start()
        
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            my_observer.stop()
            my_observer.join()
        