# CatchALL-Backup-Process

Processes done in Backup
1.  Syncing to and from Google Drive Folder
2.  Listening to new files form the Google Drive folder
3.  Copying of Video to the backup folder

To install dependencies:
pip install -r requirements.txt

How to run:
Create a folder named 'CatchALL'. This is where the files will be received.
Create a folder name 'BACKUP'. This is where all the files will be backed up to.
Edit sql_host variable in staging.py to the corresponding IP of the database